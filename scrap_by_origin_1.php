<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', 300);

require_once __DIR__.'/vendor/autoload.php';

use Goutte\Client;
 
$hahaha = new Chao(new Client());

//$cristofolcolom = range('A', 'Z'); //no serveix perque els fills de puta aprofiten les URL per SEO en algunes pagines

$scrapArrayUrl = array(
	"es_ES" 			=> "santoral", 
	"de_DE" 			=> "nombres_alemanes", 
	"es_ES" 			=> "nombres_santos", 
	"ar_AE" 			=> "nombres_arabes1", 
	"ar_AE" 			=> "nombres_arabes2", 
	"ar_AE" 			=> "nombres_arabes3", 
	"es_ES" 			=> "onomastica",
);


$plasca = array();
$final  = array();

foreach ($scrapArrayUrl as $key => $value) {

	$definedkey =  (string) $key;

	if($value == "santoral") $origen = "Santo";
	if($value == "nombres_alemanes") $origen = "Alemán";
	if($value == "nombres_santos") $origen = "Santo";
	if($value == "nombres_arabes1") $origen = "Árabe";
	if($value == "nombres_arabes2") $origen = "Árabe";
	if($value == "nombres_arabes3") $origen = "Árabe";

	$hahaha->extrarow 	= $definedkey;
	$hahaha->origen 	= $origen;

	$hahaha->unabirraja($value);
	$plasca["page"][] = $hahaha->holabondia();
}

$str     = str_replace('\u','u',$plasca);
echo  preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', json_encode($str));


Class Chao{

	public $extrarow;
	public $origen;

	function __construct(Client $client){
		$this->client = $client;
		$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 60);
	}

	public function unabirraja($var){

		$url = 'http://www.guiainfantil.com/servicios/nombres/'.$var.'.htm';

		$crawler = $this->client->request('GET', $url);
		$this->configurationRows = $crawler->filter('.tabla_nombres > tbody > tr');

	}

	public function holabondia(){

		$results = $this->configurationRows->each(function($configurationRow, $index) {

		   $noms["nom"] 		= html_entity_decode($configurationRow->filter('td')->eq(0)->text());
		   $noms["significat"] 	= $configurationRow->filter('td')->eq(1)->text();
		   $noms["origen"] 		= $this->origen;
		   $noms["sex"] 		= ($configurationRow->filter('td')->eq(1)->text()) ? $configurationRow->filter('td')->eq(1)->text() : "null";
		   $noms["lang"] 		= $this->extrarow;
		   

		   return $noms;

		});

		return $results;

	}

}

