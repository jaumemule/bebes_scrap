<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
ini_set('max_execution_time', 1000);

require_once __DIR__.'/vendor/autoload.php';

$string 		= file_get_contents("merged_letter_culture.json");
$ambsinificat 	= json_decode($string);


$string 		= file_get_contents("INE.json");
$ambsexe 		= json_decode($string);

$merge = array();

foreach ($ambsinificat as $keyold => $valueold) {

	foreach ($ambsinificat[$keyold] as $keynew => $valuenew) {

		//if($valuenew->nom != null && $valuenew->origen != null){

			foreach ($ambsexe as $key => $value) {

				if(strtolower($value->name) == strtolower($valuenew->nom)){
					$merge[$key]["nom"] 			= $value->name;
					$merge[$key]["sexe"]			= $value->sex;
					$merge[$key]["significat"]		= $valuenew->significat;
					$merge[$key]["origen"]			= $valuenew->origen;
					$merge[$key]["lang"]			= ( @$valuenew->lang ) ? $valuenew->lang : "es_ES";
				}
				/*}else{
					$merge[$key]["nom"] 			= ( $valuenew->nom ) ? ucwords(strtolower($valuenew->nom)) : null;
					$merge[$key]["sexe"]			= null;
					$merge[$key]["significat"]		= ( @$valuenew->significat ) ? $valuenew->significat : null;
					$merge[$key]["origen"]			= ( @$valuenew->origen ) ? $valuenew->origen : null;
					$merge[$key]["lang"]			= ( @$valuenew->lang ) ? $valuenew->lang : "es_ES";
				}*/
			}
		//}
	}
}


//var_dump(array_values($merge));


$file = fopen("merged.txt","w");

$str     = str_replace('\u','u',$merge);

echo fwrite($file, json_encode($str)). " iterations: ";
//echo fwrite($file, preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', json_encode($str))). " iterations: ";
fclose($file);

echo "File generated";
