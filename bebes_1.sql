-- MySQL Script generated by MySQL Workbench
-- 03/30/15 20:27:10
-- Model: New Model    Version: 1.0
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bebes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bebes` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bebes` ;

-- -----------------------------------------------------
-- Table `bebes`.`name`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`name` (
  `name_id` INT NOT NULL AUTO_INCREMENT,
  `sex` TINYINT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `language` VARCHAR(10) NOT NULL DEFAULT 'not_set',
  PRIMARY KEY (`name_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`meaning`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`meaning` (
  `meaning_id` INT NOT NULL AUTO_INCREMENT,
  `meaning` TEXT NOT NULL,
  PRIMARY KEY (`meaning_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`name_meaning`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`name_meaning` (
  `name_name_id` INT NOT NULL,
  `meaning_meaning_id` INT NOT NULL,
  INDEX `fk_name_has_meaning_meaning1_idx` (`meaning_meaning_id` ASC),
  INDEX `fk_name_has_meaning_name1_idx` (`name_name_id` ASC),
  CONSTRAINT `fk_name_has_meaning_name1`
    FOREIGN KEY (`name_name_id`)
    REFERENCES `bebes`.`name` (`name_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_name_has_meaning_meaning1`
    FOREIGN KEY (`meaning_meaning_id`)
    REFERENCES `bebes`.`meaning` (`meaning_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`tradition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`tradition` (
  `tradition_id` INT NOT NULL AUTO_INCREMENT,
  `tradition` VARCHAR(245) NULL,
  PRIMARY KEY (`tradition_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`name_tradition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`name_tradition` (
  `name_name_id` INT NOT NULL,
  `tradition_tradition_id` INT NOT NULL,
  INDEX `fk_name_has_tradition_tradition1_idx` (`tradition_tradition_id` ASC),
  INDEX `fk_name_has_tradition_name1_idx` (`name_name_id` ASC),
  CONSTRAINT `fk_name_has_tradition_name1`
    FOREIGN KEY (`name_name_id`)
    REFERENCES `bebes`.`name` (`name_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_name_has_tradition_tradition1`
    FOREIGN KEY (`tradition_tradition_id`)
    REFERENCES `bebes`.`tradition` (`tradition_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(200) NOT NULL,
  `sex` TINYINT(1) NOT NULL,
  `firstname` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`mention`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`mention` (
  `mention_id` INT NOT NULL AUTO_INCREMENT,
  `quest_id` INT NOT NULL,
  `like` TINYINT(1) NOT NULL,
  `user_id` INT NOT NULL,
  INDEX `fk_mentions_name1_idx` (`quest_id` ASC),
  PRIMARY KEY (`mention_id`),
  INDEX `fk_mentions_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_mentions_name1`
    FOREIGN KEY (`quest_id`)
    REFERENCES `bebes`.`name` (`name_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mentions_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bebes`.`user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bebes`.`quest`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bebes`.`quest` (
  `quest_id` INT NOT NULL AUTO_INCREMENT,
  `mentions_mention_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `title` VARCHAR(200) NOT NULL,
  `description` TEXT NOT NULL,
  INDEX `fk_mentions_has_users_users1_idx` (`user_id` ASC),
  INDEX `fk_mentions_has_users_mentions1_idx` (`mentions_mention_id` ASC),
  CONSTRAINT `fk_mentions_has_users_mentions1`
    FOREIGN KEY (`mentions_mention_id`)
    REFERENCES `bebes`.`mention` (`mention_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mentions_has_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bebes`.`user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
