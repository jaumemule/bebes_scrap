<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("db.php");
$string         = file_get_contents("merged.json");
$file           = (array) json_decode($string);

$file = array_values($file);

use PDO;
$connection   = new PdoDatabase();

//var_dump($connection->selectAllFromTable("name"));

foreach ($file as $key => $element) {

    $sexe       = ($element->sexe == "male") ? 1 : 2;

    $data       = array('nom' => $element->nom, "sexe" => $sexe, "lang" => $element->lang);

    $query     = "INSERT INTO name set name = :nom, sex = :sexe, language = :lang";
    $nameID    = $connection->insertInTable($query, $data);
    echo "Successfully inserted name in ID: " . $nameID.", ";

    //meaning and relation
    $significat = ($element->significat == null) ? "null" : $element->significat;
    $meaning    = array('meaning' => $significat);
    $query      = "INSERT INTO meaning set meaning = :meaning";
    $meaningID  = $connection->insertInTable($query, $meaning);

    $relation   = array('meaningID' => $meaningID, 'nameID' => $nameID);
    $query      = "INSERT INTO name_meaning set meaning_meaning_id = :meaningID, name_name_id = :nameID";
    $relation   = $connection->insertInTable($query, $relation);

    //tradition / origin and relation

    $search     = array('origin' => $element->origen);
    $querysel   = "SELECT * FROM tradition WHERE tradition = :origin";
    $check      = $connection->selectFromTable($querysel, $search);

    if(empty($check)){

        $meaning    = array('tradition' => $element->origen);
        $query      = "INSERT INTO tradition set tradition = :tradition";
        $traditionID = $connection->insertInTable($query, $meaning);

    }else{
        $traditionID = $check[0]["tradition_id"];
    }


    $relation   = array('traditionID' => $traditionID, 'nameID' => $nameID);
    $query      = "INSERT INTO name_tradition set tradition_tradition_id = :traditionID, name_name_id = :nameID";
    $tradition_rel   = $connection->insertInTable($query, $relation);


    

}

